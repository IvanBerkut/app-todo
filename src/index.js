import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import reportWebVitals from './reportWebVitals'
import { ParamProvider } from 'react-router-dom-search-params'

import { Provider } from 'react-redux'
import { createStore } from 'redux'
import todosReducer from './reducers/todos'

import { Router } from "react-router-dom"
import {createBrowserHistory} from 'history'

const history = createBrowserHistory()

const persistedState = localStorage.getItem('tasks') ? JSON.parse(localStorage.getItem('tasks')) : []
const store = createStore(todosReducer, persistedState)

store.subscribe(() => {
  localStorage.setItem('tasks', JSON.stringify(store.getState()))
})


ReactDOM.render(
  <React.StrictMode>
      <ParamProvider keep={['tag']}>
        <Provider store={store}>
          <Router history={history}>
            <App />
          </Router>
        </Provider> 
      </ParamProvider> 
  </React.StrictMode>,
  document.getElementById('root')
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
