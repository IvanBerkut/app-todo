import moment from 'moment'
const initialState = JSON.parse(localStorage.getItem('tasks')) ? JSON.parse(localStorage.getItem('tasks')) : []

const nextTodoId = (todos) => {
    const maxId = todos.reduce((maxId, todo) => Math.max(todo.taskId, maxId), -1)
    return maxId + 1
  }

const todosReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'CREATE':
            return [
                ...state,
                Object.assign(
                action.payload,
                {
                    taskId: nextTodoId(state),
                    finished: false,
                    create_at: moment().format('MMMM Do YYYY, h:mm:ss a'),
                    updated_at: "Not Yet"
                }),
            ]
        case 'EDIT':
            return state.map((todo) => {
                if (todo.taskId != action.payload.currentTaskId) {
                    return todo
                }
                return Object.assign(
                    {
                        ...todo,
                        updated_at: moment().format('MMMM Do YYYY, h:mm:ss a')
                    }, 
                    action.payload.data
                )
            })
        case 'TOGGLE':
            return state.map((todo) => {
                if (todo.taskId !== action.payload) {
                    return todo
                }

                return {
                    ...todo,
                    finished: !todo.finished
                }
            })
        case 'DELETE': {
            return state.filter((todo) => todo.taskId !== action.payload)
        }
        default:
            return state
    }
}

export default todosReducer