import './App.css'

import { HTML5Backend } from 'react-dnd-html5-backend'
import { DndProvider } from 'react-dnd'
import Header from './components/Header'
import Main from './components/Main'

const App = () => {
  return (
    <div className="App">
      <DndProvider backend={HTML5Backend}>
        <Header />
        <Main />
      </DndProvider> 
    </div>
  )
}

export default App
