import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import { useForm } from 'react-hook-form'

const selectTodoById = (state, taskId) => {
    return state.find((todo) => todo.taskId == taskId)
}

const EditTodo = ({ match, history }) => {
    const { params: { currentTaskId } } = match
    const todo = useSelector((state) => selectTodoById(state, currentTaskId))
    const { task, description, tags } = todo

    const dispatch = useDispatch()
    
    const { register, handleSubmit, errors } = useForm()
    
    const onSubmit = (data, e) => {
          dispatch({ type: 'EDIT', payload: {data, currentTaskId}})
          history.push('/')
      }

    return (
        <div>
            <h1>Create todo</h1>
            <div>
                <div className="create-task-form"> 
                    <form onSubmit={handleSubmit(onSubmit)}> 
                        <div className="form-group">
                            <label>Task:</label>
                            <input name="task" defaultValue={task} className="form-control" placeholder="Some task" ref={register({required: true})}/>
                            {errors.task && <small className="text-danger">Please, add some task</small>}
                        </div>
                        <div className="form-group">
                            <label>Description:</label>
                            <input name="description" defaultValue={description} className="form-control" placeholder="Description" ref={register}/>
                            <small className="form-text text-muted">Add some description for your task</small>
                        </div>
                        <div className="form-group">
                            <label>Tags:</label>
                            <input name="tags" defaultValue={tags} className="form-control" placeholder="Some tags" ref={register}/>
                            <small className="form-text text-muted">Add some tags for your task</small>
                        </div>
                        <button className="btn btn-primary"> Save changes </button>
                    </form>
                </div>
                <div className="btn-back">
                    <Link className="btn btn-secondary" to="/">Back</Link>
                </div>
            </div>
        </div>
    )
}
export default EditTodo
