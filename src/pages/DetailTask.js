import React from 'react'
import { useSelector } from 'react-redux'
import { Link }from 'react-router-dom'

const selectTodoById = (state, taskId) => {
    return state.find((todo) => todo.taskId == taskId)
}

const DetailTask = ({ match }) => {
    const { params: { currentTaskId } } = match
    const todo = useSelector((state) => selectTodoById(state, currentTaskId))
    const { task, description, tags, create_at, updated_at } = todo

    return (
        <div>
            <div>
                <div className="text-center"><h2>{todo.task}</h2></div>
                <div>
                    <table className="table">
                        <tbody>
                            <tr>
                                <th>Task</th>
                                <td>{task}</td>
                            </tr>
                            <tr>
                                <th>Description</th>
                                <td>{description}</td>
                            </tr>
                            <tr>
                                <th>Tags</th>
                                <td>{tags}</td>
                            </tr>
                            <tr>
                                <th>Created</th>
                                <td> {create_at}</td>
                            </tr>
                            <tr>
                                <th>Updated</th>
                                <td>{updated_at}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <Link to={`/${currentTaskId}/edit`}>Edit</Link>
                </div>
                <div className="btn-back">
                    <Link className="btn btn-secondary" to="/">Back</Link>
                </div>
            </div>
        </div>
    )
}

export default DetailTask 