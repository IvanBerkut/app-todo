import React from 'react'
import { useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import { useForm } from 'react-hook-form'

const CreateTodo = ({history}) => {
    const dispatch = useDispatch()
    

    const { register, handleSubmit, errors } = useForm()
    
    const onSubmit = (data, e) => {
          dispatch({ type: 'CREATE', payload: data})
          e.target.reset()
          history.push('/')
      }

    return (
        <div>
            <h1>Create todo</h1>
            <div>
                <div className="create-task-form"> 
                    <form onSubmit={handleSubmit(onSubmit)}> 
                        <div className="form-group">
                            <label>Task:</label>
                            <input name="task" defaultValue="" className="form-control" placeholder="Some task" ref={register({required: true})}/>
                            {errors.task && <small className="text-danger">Please, add some task</small>}
                        </div>
                        <div className="form-group">
                            <label>Description:</label>
                            <input name="description" defaultValue="" className="form-control" placeholder="Description" ref={register}/>
                            <small className="form-text text-muted">Add some description for your task</small>
                        </div>
                        <div className="form-group">
                            <label>Tags:</label>
                            <input name="tags" defaultValue="" className="form-control" placeholder="Some tags" ref={register}/>
                            <small className="form-text text-muted">Add some tags for your task</small>
                        </div>
                        <button className="btn btn-primary"> Add task </button>
                    </form>
                </div>
                <div className="btn-back">
                    <Link className="btn btn-secondary" to="/">Back</Link>
                </div>
            </div>
        </div>
    )
}

export default CreateTodo
