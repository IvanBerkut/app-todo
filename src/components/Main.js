import React from 'react'
import CreateTodo from '../pages/CreateTodo'
import DetailTask from '../pages/DetailTask'
import EditTodo from '../pages/EditTodo'
import Home from '../pages/Home'

import {
  Route,
  Switch,
  Redirect,
  withRouter
} from "react-router-dom"


function Main({history}) {
    return (
        <div>
            <Switch>
                <Route history={history} path='/tasks' component={Home} />
                <Route history={history} path='/create' component={CreateTodo} />

                <Route history={history} path='/task/:currentTaskId' component={DetailTask} />
                <Route history={history} path='/:currentTaskId/edit' component={EditTodo} />
                
                <Redirect from='/' to='/tasks'/>
            </Switch> 
        </div>
    )
}

export default withRouter(Main);