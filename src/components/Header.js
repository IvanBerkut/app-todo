import React from 'react'
import { Link } from "react-router-dom"

function Header() {
    return (
        <header>
            <nav>
                <ul className="nav">
                    <li className="nav-item">
                        <Link className="nav-link" to="/tasks">Home</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="/create">Create Task</Link>
                    </li>
                </ul>
            </nav>
        </header>
    )
}

export default Header;