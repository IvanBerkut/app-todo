import React from 'react'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { useSearchParams } from 'react-router-dom-search-params'


import TodoListItem from './TodoListItem'

const TodoList = () => {
    const searchParams = useSearchParams()
    const [ tag, setTag ] = searchParams.param('tag', '')

    const todo = useSelector((state) => state)

    const filteredTodo = todo.filter(function (str) {
        return str.tags.includes(tag)
      })

      const renderedListItems = filteredTodo.map((item) => {
          return <TodoListItem key={item.taskId} id={item.taskId}/>
      })

    return (
        <> 
            <div>
                <label>Enter tag for search: </label>
                <input className="form-control" type="text" value={tag} placeholder="Enter some tag here" onChange={e => setTag(e.target.value)} />
            </div>
            <br/>
            <table className="table">
                <thead>
                    <tr>
                        <th scope='col'>Completed</th>
                        <th scope='col'>Task</th>
                        <th scope='col'>Tags</th>
                        <th scope='col'>Delete</th>
                        <th scope='col'></th>
                    </tr>
                </thead>
                <tbody>
                    {renderedListItems}
                </tbody>
            </table>
        {renderedListItems.length > 0 ? null : <div>
            You have no any tasks at this moment. You can create <Link to="/create">Here</Link>.
        </div>}
            
        </>
    )
}

export default TodoList