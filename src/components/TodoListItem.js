import React from 'react'
import { Link } from 'react-router-dom'
 
import { useSelector, useDispatch } from 'react-redux'


const selectTodoById = (state, taskId) => {
    return state.find((todo) => todo.taskId === taskId)
}

const TodoListItem = ({ id }) => {
    const todo = useSelector((state) => selectTodoById(state, id))
    const { task, tags, finished, taskId } = todo

    const dispatch = useDispatch()

    const handleCompletedChanged = () => {
        dispatch({ type: 'TOGGLE', payload: taskId})
    }

    const onDelete = () => {
        dispatch({ type: 'DELETE', payload: taskId })
      }

    return (
        <tr className={finished ? "finished" : ""}>
            <th>
                <label className="checkbox">
                    <input onChange={handleCompletedChanged} type="checkbox" checked={finished} />
                </label>    
            </th>
            <td>{task}</td> 
            <td>{tags}</td> 
            <td><button className="btn btn-danger" onClick={onDelete}>-</button></td>
            <td><Link to={`/task/${taskId}`}>Detail</Link></td>
        </tr>
        )
}

export default TodoListItem